# 将Dto属性投影到Entity之ModifyByDto扩展



**问题场景：**

在使用某个Orm框架更新实体时，我们需要传递一个实体进去才能进行更新（有的Orm框架支持根据Dto更新数据，如：FreeSql，但有的不支持，如：EFCore），但前端传给我们的往往是一个dto对象，如下：

实体和请求参数定义：

```csharp
public class Person
{
    public int Id{set;get;}
    public string Name{set;get;}
    public double Score{set;get;}
    //...隐藏20列
    public DateTime CreateTime {set;get;}
    public bool IsDeleted {set;get;}
}

//在下面的更新中，我们仅希望更新Score，所以dto定义如下
public class UpdatePersonReq
{
    public int Id{set;get;}
    public string Name{set;get;}
    public double Score{set;get;}
    //...隐藏20列
}
```

显然，我们是想更新Person的数据，但不想更新CreateTime和Score列的值。

基于Orm框架的更新方法中只允许传入实体，所以我们的代码可能如下：

```csharp
//更新方法
public void UpdatePerson(UpdatePersonReq req)
{
	var ent =  repository.Get(req.Id);
    ent.Name=req.Name;
    ent.Score=req.Score;
    //...隐藏20列
    repository.Update(ent);
}
```

像上面的写法就浪费时间和屏幕空间。

或者我们干脆直接使用实体接收用户请求，如：

```csharp
//更新方法
public void UpdatePerson(Person reqEntity)
{
    //获取实体仅为了保证CreateTime和IsDeleted不被覆盖
	var tmp =  repository.Get(req.Id);    
    reqEntity.CreateTime=tmp.CreateTime;    
    reqEntity.IsDeleted=tmp.IsDeleted;
    
    //...隐藏20列
    //使用请求参数更新
    repository.Update(reqEntity);
}
```

虽然我们使用实体接收用户请求看似解决了问题，但这样的反向操作不够直观，而且对于前后端分离的项目，swagger将会把实体的所有字段全部暴露出去（包括CreateTime和IsDeleted）。



为了解决以上问题，这里封装了ModifyByDto扩展方法，利用此方法可以方便的将Dto中的属性值投影到实体中，达到改变实体属性值的目的。

如下所示：

```csharp
//更新方法
public void UpdatePerson(UpdatePersonReq req)
{
	var ent =  repository.Get(req.Id);
    //使用req中的属性值修改ent
    ent.ModifyByDto(req);
    repository.Update(ent);
}
```

可以看到，利用ModifyByDto完美的解决了这个问题。



**ModifyByDto的特点：**

- 通Mapper、Modify扩展方法一样，基于反射实现；

- 遍历属性是先判断是否相等，不相等的话才会修改；

- 可以递归遍历属性值，如下所示：

  ```csharp
  public class Person
  {
      public int Id{set;get;}
      public string Name{set;get;}
      public Student Student{set;get;}
  }
  public class Student
  {
      public int Id{set;get;}
      public string Name{set;get;}
  }
  //使用示例
  void Test()
  {
      Person person=null;
      //对于实体类属性，递归修改其值
      person.ModifyByDto(new
  	{
       	Id=1,
          Name="小明",
          Student=new 
          {
              Id=2,
         		Name="小红",
          }
      });
  }
  ```

- 对于集合属性值，直接覆盖，如下所示：

  ```csharp
  public class Person
  {
      public int Id{set;get;}
      public string Name{set;get;}
      public List<string> Books{set;get;}
  }
  //使用示例
  void Test()
  {
      Person person=null;
      //对于实体类属性，递归修改其值
      person.ModifyByDto(new
  	{
       	Id=1,
          Name="小明",
          //dto中的Books会整体替换掉实体中的Books
          Books=new List<string>(){"语文","数学"}
      });
  }
  ```

- 执行ModifyByDto方法的对象不能是一个集合，也就是说下面的会报错：

  ```csharp
  var list=new List<Person>();
  //不能集合上直接调用ModifyByDto
  list.ModifyByDto(new{});
  ```

  