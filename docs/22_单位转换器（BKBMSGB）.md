# 单位转换器（BKBMSGB）

这里封装了字节B、KB、MB、GB、TB直接的转换。

直接看代码：

```csharp
//输出: 12 B
var str = UnitConverter.Humanize(12d);
//输出: 1 KB
str = UnitConverter.Humanize(1024.23d);
//输出: 53.01 GB
str = UnitConverter.Humanize(1024.23 * 1024 * 1024 * 53d);
//B、KB、MB、GB、TB相互转换
//GB转MB,输出: 54282.24 MB
str = UnitConverter.GigaBytesToMegaBytes(53.01d) + " MB";
```


