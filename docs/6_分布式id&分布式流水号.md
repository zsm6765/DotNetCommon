# 分布式id&分布式流水号



## 1. 集群环境中如何标识机器唯一

这里使用了两个值：MachineId、MachineIdString，前者是int类型的被用在分布式id生成中，后者则是string类型的被用在分布式流水号生成中。

这两个值都被记录在类```Machine```的静态属性中，在程序内唯一。



设置方法如下：

```csharp
//设置MachineId为5，MachineIdString为null
Machine.SetMachineId(5, null);
//同时设置MachineId和MachineIdString
Machine.SetMachineId(5, "ABCD");
//当然，也可以将两个值设置一样的
Machine.SetMachineId(5, "5");
```



如果觉得上面的设置比较麻烦，可以使用下面的方法自动从环境变量和程序的运行目录下读取配置：

```csharp
//分别从环境变量和程序运行目录下的"machine.txt"文件中去读配置，"machine.txt"的优先级高
//machine.txt中的格式为:
/*
MachineId=12
MachineIdString=ABCD
*/
Machine.SetMachineFlagByDefault();
```



自动设置： 类```Machine```的静态构造函数中已经调用了```SetMachineFlagByDefault()```方法，所以除非你想使用```SetMachineId()```自定义机器Id值，则无须调用设置方法。

```csharp
static Machine()
{
    SetMachineFlagByDefault();
}
```



**注意：** 在分布式流水号生成中如果发现未设置```MachineIdString```的值，那么将使用```MachineId```替代。



## 2. 分布式Id生成之雪花算法

原始的雪花算法规则是使用long型整数表示一个Id（8字节，64bit位）。

- 将这64个bit位分为：1 + 41 + 10 +12 四部分。
- 1bit：固定为1，表示正整数；
- 41bit：记录从基准时间到现在的毫秒数，最多可表示69年；
- 10bit：机器Id，最多可表示1024台机器；
- 12bit：序列号，最大并发，4096/ms，即：409万QPS；



这里对它进行了扩展，扩展后新增的特点：

- 可自定义时间精度，秒或毫秒；
- 可自定义时间戳位数；
- 可自定义机器Id位数；
- 可根据Id反向解析时间_机器Id_序列号；



这个雪花算法用类```SnowflakeIdWorker```表示，下面是使用示例：

```csharp
//设置机器Id为1,其他的使用默认值: 时间精度-毫秒，起始时间-2020-01-01，机器id的bit位长度-10，序列号bit位长度-12
var work = new SnowflakeIdWorker(1);
//work = new SnowflakeIdWorker(1,DateTime.Parse("2021-01-01"), SnowflakeIdWorker.WorkerTimeModel.Millisecond,41,10,12);

var dic = new Dictionary<long, string>();
for (var i = 0; i < 20000; i++)
{
    dic.Add(work.NextId(), null);
}
var dic2 = new Dictionary<long, string>();
foreach (var i in dic)
{
    var id = i.Key;
    dic2[id] = work.AnalyzeId(id);
}
```



为了方便调用，这里对```SnowflakeIdWorker```又做了一层封装，使用如下：

```csharp
//DistributeGenerator内部缓存了以testkey为键，以new SnowflakeIdWorker(Machine.MachineId)为值得字典
long id=DistributeGenerator.NewId("testkey");
```





**注意：** 关于雪花算法的参数选择参照表：

*时间位数：*

| bit位 | 年（秒） |
| ----- | -------- |
| 34    | 544年    |
| 33    | 272年    |
| 32    | 136年    |
| 31    | 68年     |
| 30    | 34年     |
| 29    | 17年     |
| 28    | 8.5年    |



| bit位 | 年（毫秒） |
| ----- | ---------- |
| 44    | 557年      |
| 43    | 278年      |
| 42    | 139年      |
| 41    | 69年       |
| 40    | 34年       |
| 39    | 17年       |
| 28    | 8.7年      |



*机器Id的bit位数：*

| bit位 | 机器数  |
| ----- | ------- |
| 18    | 26万台  |
| 17    | 13万台  |
| 16    | 6万台   |
| 15    | 3万台   |
| 14    | 1.6万台 |
| 13    | 8192台  |
| 12    | 4096台  |
| 11    | 2048台  |
| 10    | 1024台  |
| 9     | 512台   |
| 8     | 256台   |
| 7     | 128台   |
| 6     | 64台    |
| 5     | 32台    |



*序列号的bit位数：*

| bit位 | 并发（秒） |
| ----- | ---------- |
| 24    | 1677万/s   |
| 23    | 838万/s    |
| 22    | 419万/s    |
| 21    | 209万/s    |
| 20    | 104万/s    |
| 19    | 52万/s     |
| 18    | 26万/s     |
| 17    | 13万/s     |
| 16    | 6万/s      |
| 15    | 3万/s      |
| 14    | 1.6万/s    |
| 13    | 8192/s     |
| 12    | 4096/s     |



| bit位 | 并发（毫秒） |
| ----- | ------------ |
| 14    | 1638万/s     |
| 13    | 819万/s      |
| 12    | 409万/s      |
| 11    | 204万/s      |
| 10    | 102万/s      |
| 9     | 51万/s       |
| 8     | 25万/s       |
| 7     | 12万/s       |
| 6     | 6万/s        |
| 5     | 3万/s        |
| 4     | 1.6万/s      |
| 3     | 8000/s       |
| 2     | 4000/s       |



## 3. 分布式流水号

先看下生成的示例：

SNO20200604ABCD0001： “SNO”为固定前缀，"20200601"为时间戳，“ABCD”为机器Id串，"0001"为序列号

ABCD-20200604-0001:  将机器Id串提前,加入分割符

202006040001：没有固定前缀，也没有机器Id串



为了生成上面的流水号，首先需要设置Machine的MachineIdString为“ABCD”，然后调用下列的方法：

```csharp
//指定前缀为SNO，时间戳格式，序列号长度，并启用分布式Id串
//SNO20200604ABCD0001
DistributeGenerator.NewSNO("testkey", SerialFormat.CreateFast("SNO", "yyyyMMdd", 4, true));

//ABCD-20200604-0001
DistributeGenerator.NewSNO("testdb_order4", SerialFormat.CreateByChunks(new List<SerialFormatChunk>()
{
    new SerialFormatChunk()
    {
        Type=SerialFormatChunkType.MachineText
    },
    new SerialFormatChunk()
    {
        Type=SerialFormatChunkType.StaticText,
        FormatString="-"
    },
    new SerialFormatChunk()
    {
        Type=SerialFormatChunkType.DateText,
        FormatString="yyyyMMdd"
    },
    new SerialFormatChunk()
    {
        Type=SerialFormatChunkType.StaticText,
        FormatString="-"
    },
    new SerialFormatChunk()
    {
        Type=SerialFormatChunkType.SerialNo,
        Length=4
    }
}));

//202006040001
DistributeGenerator.NewSNO("testdb_order5", SerialFormat.CreateByChunks(new List<SerialFormatChunk>()
{
    new SerialFormatChunk()
    {
        Type=SerialFormatChunkType.DateText,
        FormatString="yyyyMMdd"
    },
    new SerialFormatChunk()
    {
        Type=SerialFormatChunkType.SerialNo,
        Length=4
    }
}));
```



**如何控制流水号生成的格式？**

这里设计使用一个格式块集合表示流水号的格式。

格式块用类```SerialFormatChunk```表示，共用四种类型的块，用枚举```SerialFormatChunkType```表示，格式块的定义如下：

```csharp
public class SerialFormatChunk
{
    //有静态文本，时间戳，机器Id串，序列号四种类型
    public SerialFormatChunkType Type { set; get; }
    //当格式块类型为静态文本时，该值被直接拼接到流水号中，当类型为时间戳时则使用DateTime.Now.ToString(FormatString)的结果拼接
    public string FormatString { set; get; }
    //当格式块类型为序列号时，控制序列号的长度，不够的左侧补齐0，如果超出，则流水号长度自动延长（正因为如此，序列号块比如放在最后）
    public int Length { set; get; }
}
```



为了组合这几个格式块，定义了类```SerialFormat```进行表示，这个类里面除了封装了格式块集合，还提供了便捷的操作方法，如：

```csharp
SerialFormat.CreateFast(...);
SerialFormat.CreateDistributeFast(...);
SerialFormat.CreateByChunks(...);
```



这几个格式串组合的时候要遵循如下规则：

- 有且只能有一个日期格式块；
- 有且只能有一个序列号块，且必须放在块集合的最后；
- 机器Id串的块可已有0或多个；
- 静态文本块可以有0或多个；
- 生成流水号时，按照块顺序进行组装；

