﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DotNetCommon.Data
{
    /// <summary>
    /// 树状接口(区别于TreeNode模型)
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface ITreeStruct<T>
    {
        /// <summary>
        /// 子级
        /// </summary>
        List<T> Children { set; get; }
    }
}
