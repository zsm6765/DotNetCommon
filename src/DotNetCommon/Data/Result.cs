﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using DotNetCommon.Extensions;

namespace DotNetCommon.Data
{
    /// <summary>
    /// 通用结果模型
    /// </summary>
    [JsonObject(MemberSerialization.OptOut)]
    [DataContract]
    public class Result<T>
    {
        /// <summary>
        /// 状态码,一般用来表示错误类别
        /// </summary>
        [DataMember(Order = 1)]
        public int Code { get; set; }

        /// <summary>
        /// 消息文本,一般用来做错误提示
        /// </summary>
        [DataMember(Order = 2)]
        public string Message { get; set; }

        /// <summary>
        /// 详细消息文本,可用来做详细的错误提示
        /// </summary>
        [DataMember(Order = 3)]
        public object ExtData { get; set; }

        /// <summary>
        /// 执行成功或失败
        /// </summary>
        [DataMember(Order = 4)]
        public bool Success { get; set; }

        /// <summary>
        /// 执行的结果数据
        /// </summary>
        [DataMember(Order = 5)]
        public T Data { get; set; }

        /// <summary>
        /// 隐示转换
        /// </summary>
        /// <param name="res"></param>
        public static implicit operator Result<T>(Result res)
        {
            return new Result<T>()
            {
                Code = res.Code,
                Message = res.Message,
                ExtData = res.ExtData,
                Success = res.Success,
                Data = default(T)
            };
        }
    }

    /// <summary>
    /// 以object作为结果数据类别,提供静态快捷方法
    /// </summary>
    [DataContract]
    public class Result : Result<object>
    {
        /// <summary>
        /// 返回成功,使用json序列化机制转换到目标类型
        /// </summary>
        /// <param name="value">模型数据</param>
        /// <returns></returns>
        public static Result<T> Ok<T>(object value)
        {
            var json = Newtonsoft.Json.JsonConvert.SerializeObject(value);
            var t = Newtonsoft.Json.JsonConvert.DeserializeObject<T>(json);
            return new Result<T>()
            {
                Success = true,
                Data = t
            };
        }

        /// <summary>
        /// 返回成功
        /// </summary>
        /// <param name="value">模型数据</param>
        /// <returns></returns>
        public static Result<T> Ok<T>(T value)
        {
            return new Result<T>()
            {
                Success = true,
                Data = value
            };
        }

        /// <summary>
        /// 返回成功,不需要附带模型数据
        /// </summary>
        /// <returns></returns>
        public static Result Ok()
        {
            return new Result()
            {
                Success = true
            };
        }

        /// <summary>
        /// 返回分页模型数据
        /// </summary>
        /// <param name="totalCount">数据总量</param>
        /// <param name="list">当前页码内的数据</param>
        /// <returns></returns>
        public static ResultPage<T> OkPage<T>(long totalCount, IEnumerable<T> list)
        {
            return new ResultPage<T>()
            {
                Success = true,
                Data = new Page<T>()
                {
                    TotalCount = totalCount,
                    List = list
                }
            };
        }

        /// <summary>
        /// 返回分页模型数据
        /// </summary>
        /// <param name="totalCount">数据总量</param>
        /// <param name="list">当前页码内数量</param>
        /// <returns></returns>
        public static ResultPage<T> OkPage<T>(long totalCount, object[] list) where T : class, new()
        {
            if (typeof(T) == typeof(object))
            {
                return new ResultPage<T>()
                {
                    Success = true,
                    Data = new Page<T>()
                    {
                        TotalCount = totalCount,
                        List = (T[])list
                    }
                };
            }
            return new ResultPage<T>()
            {
                Success = true,
                Data = new Page<T>()
                {
                    TotalCount = totalCount,
                    List = list.Mapper<List<T>>()
                }
            };
        }

        /// <summary>
        /// 返回失败结果
        /// </summary>
        /// <param name="message">失败提示</param>
        /// <param name="code">错误码</param>
        /// <param name="data">模型数据</param>
        /// <param name="extData">扩展数据</param>
        /// <returns></returns>
        public static Result NotOk(string message, int code = 0, object data = null, object extData = null)
        {
            return new Result()
            {
                Success = false,
                Message = message,
                ExtData = extData,
                Code = code,
                Data = data
            };
        }
    }

    /// <summary>
    /// Result扩展类
    /// </summary>
    public static class ResultExtensions
    {
        /// <summary>
        /// 解开Result模型,null值则返回 default(T)
        /// Result.Success为true则返回模型,否则抛出ResultException异常
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="result"></param>
        /// <returns></returns>
        public static T UnWrap<T>(this Result<T> result)
        {
            if (result == null) return default(T);
            if (result.Success) return result.Data;
            throw new Exception(result.Message);
        }
    }
}
