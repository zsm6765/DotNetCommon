﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace DotNetCommon.Data
{
    /// <summary>
    /// 树节点模型
    /// </summary>
    /// <typeparam name="T">模型数据</typeparam>
    [JsonObject(MemberSerialization.OptOut)]
    [DataContract]
    public sealed class TreeNode<T>
    {
        /// <summary>
        /// 节点中存储的数据
        /// </summary>
        [DataMember(Order = 1)]
        public T Value { get; }

        /// <summary>
        /// 节点的所有子节点
        /// </summary>
        [DataMember(Order = 1)]
        public IList<TreeNode<T>> Children { get; }

        /// <summary>
        /// 创建 <see cref="TreeNode{T}"/> 实例
        /// </summary>
        public TreeNode(T value, TreeNode<T>[] children)
        {
            Value = value;
            Children = children;
        }
    }
}
