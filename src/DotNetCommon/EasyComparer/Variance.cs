﻿namespace DotNetCommon.EasyComparer
{
    using System;
    using System.Collections;
    using System.Linq;
    using System.Reflection;
    using DotNetCommon.Extensions;

    /// <summary>
    /// 表示对象之间差异的抽象。
    /// </summary>
    public sealed class Variance
    {
        internal Variance(PropertyInfo property, object leftValue, object rightValue)
        {
            Property = property;
            LeftValue = leftValue;
            RightValue = rightValue;

            Varies = VariesImpl();
        }

        private bool VariesImpl()
        {
            var isSequence = Property.PropertyType.IsSequence(out SequenceType _);

            if (LeftValue is null) { return RightValue != null; }
            if (RightValue is null) { return false; }

            if (!isSequence) { return !Equals(LeftValue, RightValue); }

            var left = ((IEnumerable)LeftValue).Cast<object>();
            var right = ((IEnumerable)RightValue).Cast<object>();
            return !left.SequenceEqual(right);
        }

        /// <summary>
        /// 比较的属性信息
        /// </summary>
        public PropertyInfo Property { get; }

        /// <summary>
        /// 第一个比较的属性值
        /// </summary>
        public object LeftValue { get; }

        /// <summary>
        /// 第二个比较的属性值
        /// </summary>
        public object RightValue { get; }

        /// <summary>
        /// <see cref="RightValue"/>和<see cref="LeftValue"/> 相比是否发生了变化
        /// </summary>
        public bool Varies { get; }
    }
}