﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Text;

namespace DotNetCommon.Serialize
{
    /// <summary>
    /// 日期格式转换器,定义在属性上比较方便（ [JsonConverter(typeof(DateTimeConverter), "yyyy-MM-dd")]）
    /// </summary>
    public sealed class DateTimeConverter : IsoDateTimeConverter
    {
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="format"></param>
        public DateTimeConverter(string format) : base()
        {
            base.DateTimeFormat = format;
        }
    }
}
