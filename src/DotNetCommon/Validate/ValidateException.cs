﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DotNetCommon.Validate
{
    /// <summary>
    /// 校验异常
    /// </summary>
    public class ValidateException : Exception { }
}
