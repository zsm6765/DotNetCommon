﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DotNetCommon.Extensions;

namespace DotNetCommon.Validate
{
    /// <summary>
    /// 集合校验扩展
    /// </summary>
    public static class ValidateListExtensions
    {
        #region NotNullOrEmpty 集合不能为空
        /// <summary>
        /// 集合不能为空
        /// </summary>
        /// <param name="ctx"></param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        public static ValidateContext<IList<T>> MustNotNullOrEmpty<T>(this ValidateContext<IList<T>> ctx, string errorMessage = null)
        {
            if (!ctx.Model.IsNotNullOrEmpty())
            {
                ctx.ErrorMessage.Add((ctx.ModelPath, errorMessage ?? $"'{ctx.ModelPath}' 不能为空!"));
            }
            return ctx;
        }

        /// <summary>
        /// 集合不能为空
        /// </summary>
        /// <param name="ctx"></param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        public static ValidateContext<List<T>> MustNotNullOrEmpty<T>(this ValidateContext<List<T>> ctx, string errorMessage = null)
        {
            if (!ctx.Model.IsNotNullOrEmpty())
            {
                ctx.ErrorMessage.Add((ctx.ModelPath, errorMessage ?? $"'{ctx.ModelPath}' 不能为空!"));
            }
            return ctx;
        }

        /// <summary>
        /// 集合不能为空
        /// </summary>
        /// <param name="ctx"></param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        public static ValidateContext<IEnumerable<T>> MustNotNullOrEmpty<T>(this ValidateContext<IEnumerable<T>> ctx, string errorMessage = null)
        {
            if (!ctx.Model.IsNotNullOrEmpty())
            {
                ctx.ErrorMessage.Add((ctx.ModelPath, errorMessage ?? $"'{ctx.ModelPath}' 不能为空!"));
            }
            return ctx;
        }

        /// <summary>
        /// 集合不能为空
        /// </summary>
        /// <param name="ctx"></param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        public static ValidateContext<ICollection<T>> MustNotNullOrEmpty<T>(this ValidateContext<ICollection<T>> ctx, string errorMessage = null)
        {
            if (!ctx.Model.IsNotNullOrEmpty())
            {
                ctx.ErrorMessage.Add((ctx.ModelPath, errorMessage ?? $"'{ctx.ModelPath}' 不能为空!"));
            }
            return ctx;
        }
        #endregion

        #region MustBeNullOrEmpty 集合必须为空
        /// <summary>
        /// 集合必须为空
        /// </summary>
        /// <param name="ctx"></param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        public static ValidateContext<IList<T>> MustBeNullOrEmpty<T>(this ValidateContext<IList<T>> ctx, string errorMessage = null)
        {
            if (ctx.Model.IsNotNullOrEmpty())
            {
                ctx.ErrorMessage.Add((ctx.ModelPath, errorMessage ?? $"'{ctx.ModelPath}' 必须为空!"));
            }
            return ctx;
        }

        /// <summary>
        /// 集合必须为空
        /// </summary>
        /// <param name="ctx"></param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        public static ValidateContext<List<T>> MustBeNullOrEmpty<T>(this ValidateContext<List<T>> ctx, string errorMessage = null)
        {
            if (ctx.Model.IsNotNullOrEmpty())
            {
                ctx.ErrorMessage.Add((ctx.ModelPath, errorMessage ?? $"'{ctx.ModelPath}' 必须为空!"));
            }
            return ctx;
        }

        /// <summary>
        /// 集合必须为空
        /// </summary>
        /// <param name="ctx"></param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        public static ValidateContext<IEnumerable<T>> MustBeNullOrEmpty<T>(this ValidateContext<IEnumerable<T>> ctx, string errorMessage = null)
        {
            if (ctx.Model.IsNotNullOrEmpty())
            {
                ctx.ErrorMessage.Add((ctx.ModelPath, errorMessage ?? $"'{ctx.ModelPath}' 必须为空!"));
            }
            return ctx;
        }

        /// <summary>
        /// 集合必须为空
        /// </summary>
        /// <param name="ctx"></param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        public static ValidateContext<ICollection<T>> MustBeNullOrEmpty<T>(this ValidateContext<ICollection<T>> ctx, string errorMessage = null)
        {
            if (ctx.Model.IsNotNullOrEmpty())
            {
                ctx.ErrorMessage.Add((ctx.ModelPath, errorMessage ?? $"'{ctx.ModelPath}' 必须为空!"));
            }
            return ctx;
        }
        #endregion

        #region RuleForEach 集合遍历
        /// <summary>
        /// 集合元素
        /// </summary>
        /// <param name="ctx"></param>
        /// <param name="validater"></param>
        /// <returns></returns>
        public static ValidateContext<ICollection<T>> RuleForEach<T>(this ValidateContext<ICollection<T>> ctx, Action<ValidateContext<T>> validater)
        {
            var list = ctx.Model.ToList();
            //获取prop的name
            for (var i = 0; i < ctx.Model.Count; i++)
            {
                var propName = $"[{i}]";
                var propModel = list[i]; ;
                var newCtx = new ValidateContext<T>(propModel, propName);
                ctx.Children.Add(newCtx);
                newCtx.Parent = ctx;
                validater.Invoke(newCtx);
            }
            return ctx;
        }

        /// <summary>
        /// 集合元素
        /// </summary>
        /// <param name="ctx"></param>
        /// <param name="validater"></param>
        /// <returns></returns>
        public static ValidateContext<IList<T>> RuleForEach<T>(this ValidateContext<IList<T>> ctx, Action<ValidateContext<T>> validater)
        {
            var list = ctx.Model.ToList();
            //获取prop的name
            for (var i = 0; i < ctx.Model.Count; i++)
            {
                var propName = $"[{i}]";
                var propModel = list[i];
                var newCtx = new ValidateContext<T>(propModel, propName);
                ctx.Children.Add(newCtx);
                newCtx.Parent = ctx;
                validater.Invoke(newCtx);
            }
            return ctx;
        }

        /// <summary>
        /// 集合元素
        /// </summary>
        /// <param name="ctx"></param>
        /// <param name="validater"></param>
        /// <returns></returns>
        public static ValidateContext<List<T>> RuleForEach<T>(this ValidateContext<List<T>> ctx, Action<ValidateContext<T>> validater)
        {
            var list = ctx.Model.ToList();
            //获取prop的name
            for (var i = 0; i < ctx.Model.Count; i++)
            {
                var propName = $"[{i}]";
                var propModel = list[i];
                var newCtx = new ValidateContext<T>(propModel, propName);
                ctx.Children.Add(newCtx);
                newCtx.Parent = ctx;
                validater.Invoke(newCtx);
            }
            return ctx;
        }
        #endregion
    }
}
