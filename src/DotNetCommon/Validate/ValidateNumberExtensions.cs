﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DotNetCommon.Validate
{
    /// <summary>
    /// 数字校验扩展
    /// </summary>
    public static class ValidateNumberExtensions
    {
        #region 必须为正数(>0)
        /// <summary>
        /// 必须为正数
        /// </summary>
        /// <param name="ctx"></param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        public static ValidateContext<int?> MustGreaterThanZero(this ValidateContext<int?> ctx, string errorMessage = null)
        {
            if (ctx.Model == null || ctx.Model <= 0)
            {
                ctx.ErrorMessage.Add((ctx.ModelPath, errorMessage ?? $"'{ctx.ModelPath}' 必须大于0!"));
            }
            return ctx;
        }

        /// <summary>
        /// 必须为正数
        /// </summary>
        /// <param name="ctx"></param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        public static ValidateContext<int> MustGreaterThanZero(this ValidateContext<int> ctx, string errorMessage = null)
        {
            if (ctx.Model <= 0)
            {
                ctx.ErrorMessage.Add((ctx.ModelPath, errorMessage ?? $"'{ctx.ModelPath}' 必须大于0!"));
            }
            return ctx;
        }

        /// <summary>
        /// 必须为正数
        /// </summary>
        /// <param name="ctx"></param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        public static ValidateContext<short?> MustGreaterThanZero(this ValidateContext<short?> ctx, string errorMessage = null)
        {
            if (ctx.Model == null || ctx.Model <= 0)
            {
                ctx.ErrorMessage.Add((ctx.ModelPath, errorMessage ?? $"'{ctx.ModelPath}' 必须大于0!"));
            }
            return ctx;
        }

        /// <summary>
        /// 必须为正数
        /// </summary>
        /// <param name="ctx"></param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        public static ValidateContext<short> MustGreaterThanZero(this ValidateContext<short> ctx, string errorMessage = null)
        {
            if (ctx.Model <= 0)
            {
                ctx.ErrorMessage.Add((ctx.ModelPath, errorMessage ?? $"'{ctx.ModelPath}' 必须大于0!"));
            }
            return ctx;
        }

        /// <summary>
        /// 必须为正数
        /// </summary>
        /// <param name="ctx"></param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        public static ValidateContext<long?> MustGreaterThanZero(this ValidateContext<long?> ctx, string errorMessage = null)
        {
            if (ctx.Model == null || ctx.Model <= 0)
            {
                ctx.ErrorMessage.Add((ctx.ModelPath, errorMessage ?? $"'{ctx.ModelPath}' 必须大于0!"));
            }
            return ctx;
        }

        /// <summary>
        /// 必须为正数
        /// </summary>
        /// <param name="ctx"></param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        public static ValidateContext<long> MustGreaterThanZero(this ValidateContext<long> ctx, string errorMessage = null)
        {
            if (ctx.Model <= 0)
            {
                ctx.ErrorMessage.Add((ctx.ModelPath, errorMessage ?? $"'{ctx.ModelPath}' 必须大于0!"));
            }
            return ctx;
        }

        /// <summary>
        /// 必须为正数
        /// </summary>
        /// <param name="ctx"></param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        public static ValidateContext<float?> MustGreaterThanZero(this ValidateContext<float?> ctx, string errorMessage = null)
        {
            if (ctx.Model == null || ctx.Model <= 0)
            {
                ctx.ErrorMessage.Add((ctx.ModelPath, errorMessage ?? $"'{ctx.ModelPath}' 必须大于0!"));
            }
            return ctx;
        }

        /// <summary>
        /// 必须为正数
        /// </summary>
        /// <param name="ctx"></param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        public static ValidateContext<float> MustGreaterThanZero(this ValidateContext<float> ctx, string errorMessage = null)
        {
            if (ctx.Model <= 0)
            {
                ctx.ErrorMessage.Add((ctx.ModelPath, errorMessage ?? $"'{ctx.ModelPath}' 必须大于0!"));
            }
            return ctx;
        }

        /// <summary>
        /// 必须为正数
        /// </summary>
        /// <param name="ctx"></param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        public static ValidateContext<double?> MustGreaterThanZero(this ValidateContext<double?> ctx, string errorMessage = null)
        {
            if (ctx.Model == null || ctx.Model <= 0)
            {
                ctx.ErrorMessage.Add((ctx.ModelPath, errorMessage ?? $"'{ctx.ModelPath}' 必须大于0!"));
            }
            return ctx;
        }

        /// <summary>
        /// 必须为正数
        /// </summary>
        /// <param name="ctx"></param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        public static ValidateContext<double> MustGreaterThanZero(this ValidateContext<double> ctx, string errorMessage = null)
        {
            if (ctx.Model <= 0)
            {
                ctx.ErrorMessage.Add((ctx.ModelPath, errorMessage ?? $"'{ctx.ModelPath}' 必须大于0!"));
            }
            return ctx;
        }

        /// <summary>
        /// 必须为正数
        /// </summary>
        /// <param name="ctx"></param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        public static ValidateContext<decimal?> MustGreaterThanZero(this ValidateContext<decimal?> ctx, string errorMessage = null)
        {
            if (ctx.Model == null || ctx.Model <= 0)
            {
                ctx.ErrorMessage.Add((ctx.ModelPath, errorMessage ?? $"'{ctx.ModelPath}' 必须大于0!"));
            }
            return ctx;
        }

        /// <summary>
        /// 必须为正数
        /// </summary>
        /// <param name="ctx"></param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        public static ValidateContext<decimal> MustGreaterThanZero(this ValidateContext<decimal> ctx, string errorMessage = null)
        {
            if (ctx.Model <= 0)
            {
                ctx.ErrorMessage.Add((ctx.ModelPath, errorMessage ?? $"'{ctx.ModelPath}' 必须大于0!"));
            }
            return ctx;
        }
        #endregion

        #region 必须>=0
        /// <summary>
        /// 必须大于等于0
        /// </summary>
        /// <param name="ctx"></param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        public static ValidateContext<int?> MustGreaterThanOrEuqalToZero(this ValidateContext<int?> ctx, string errorMessage = null)
        {
            if (ctx.Model == null || ctx.Model < 0)
            {
                ctx.ErrorMessage.Add((ctx.ModelPath, errorMessage ?? $"'{ctx.ModelPath}' 必须大于等于0!"));
            }
            return ctx;
        }

        /// <summary>
        /// 必须大于等于0
        /// </summary>
        /// <param name="ctx"></param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        public static ValidateContext<int> MustGreaterThanOrEuqalToZero(this ValidateContext<int> ctx, string errorMessage = null)
        {
            if (ctx.Model < 0)
            {
                ctx.ErrorMessage.Add((ctx.ModelPath, errorMessage ?? $"'{ctx.ModelPath}' 必须大于等于0!"));
            }
            return ctx;
        }

        /// <summary>
        /// 必须大于等于0
        /// </summary>
        /// <param name="ctx"></param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        public static ValidateContext<short?> MustGreaterThanOrEuqalToZero(this ValidateContext<short?> ctx, string errorMessage = null)
        {
            if (ctx.Model == null || ctx.Model < 0)
            {
                ctx.ErrorMessage.Add((ctx.ModelPath, errorMessage ?? $"'{ctx.ModelPath}' 必须大于等于0!"));
            }
            return ctx;
        }

        /// <summary>
        /// 必须大于等于0
        /// </summary>
        /// <param name="ctx"></param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        public static ValidateContext<short> MustGreaterThanOrEuqalToZero(this ValidateContext<short> ctx, string errorMessage = null)
        {
            if (ctx.Model < 0)
            {
                ctx.ErrorMessage.Add((ctx.ModelPath, errorMessage ?? $"'{ctx.ModelPath}' 必须大于等于0!"));
            }
            return ctx;
        }

        /// <summary>
        /// 必须大于等于0
        /// </summary>
        /// <param name="ctx"></param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        public static ValidateContext<long?> MustGreaterThanOrEuqalToZero(this ValidateContext<long?> ctx, string errorMessage = null)
        {
            if (ctx.Model == null || ctx.Model < 0)
            {
                ctx.ErrorMessage.Add((ctx.ModelPath, errorMessage ?? $"'{ctx.ModelPath}' 必须大于等于0!"));
            }
            return ctx;
        }

        /// <summary>
        /// 必须大于等于0
        /// </summary>
        /// <param name="ctx"></param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        public static ValidateContext<long> MustGreaterThanOrEuqalToZero(this ValidateContext<long> ctx, string errorMessage = null)
        {
            if (ctx.Model < 0)
            {
                ctx.ErrorMessage.Add((ctx.ModelPath, errorMessage ?? $"'{ctx.ModelPath}' 必须大于等于0!"));
            }
            return ctx;
        }

        /// <summary>
        /// 必须大于等于0
        /// </summary>
        /// <param name="ctx"></param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        public static ValidateContext<float?> MustGreaterThanOrEuqalToZero(this ValidateContext<float?> ctx, string errorMessage = null)
        {
            if (ctx.Model == null || ctx.Model < 0)
            {
                ctx.ErrorMessage.Add((ctx.ModelPath, errorMessage ?? $"'{ctx.ModelPath}' 必须大于等于0!"));
            }
            return ctx;
        }

        /// <summary>
        /// 必须大于等于0
        /// </summary>
        /// <param name="ctx"></param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        public static ValidateContext<float> MustGreaterThanOrEuqalToZero(this ValidateContext<float> ctx, string errorMessage = null)
        {
            if (ctx.Model < 0)
            {
                ctx.ErrorMessage.Add((ctx.ModelPath, errorMessage ?? $"'{ctx.ModelPath}' 必须大于等于0!"));
            }
            return ctx;
        }

        /// <summary>
        /// 必须大于等于0
        /// </summary>
        /// <param name="ctx"></param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        public static ValidateContext<double?> MustGreaterThanOrEuqalToZero(this ValidateContext<double?> ctx, string errorMessage = null)
        {
            if (ctx.Model == null || ctx.Model < 0)
            {
                ctx.ErrorMessage.Add((ctx.ModelPath, errorMessage ?? $"'{ctx.ModelPath}' 必须大于等于0!"));
            }
            return ctx;
        }

        /// <summary>
        /// 必须大于等于0
        /// </summary>
        /// <param name="ctx"></param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        public static ValidateContext<double> MustGreaterThanOrEuqalToZero(this ValidateContext<double> ctx, string errorMessage = null)
        {
            if (ctx.Model < 0)
            {
                ctx.ErrorMessage.Add((ctx.ModelPath, errorMessage ?? $"'{ctx.ModelPath}' 必须大于等于0!"));
            }
            return ctx;
        }

        /// <summary>
        /// 必须大于等于0
        /// </summary>
        /// <param name="ctx"></param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        public static ValidateContext<decimal?> MustGreaterThanOrEuqalToZero(this ValidateContext<decimal?> ctx, string errorMessage = null)
        {
            if (ctx.Model == null || ctx.Model < 0)
            {
                ctx.ErrorMessage.Add((ctx.ModelPath, errorMessage ?? $"'{ctx.ModelPath}' 必须大于等于0!"));
            }
            return ctx;
        }

        /// <summary>
        /// 必须大于等于0
        /// </summary>
        /// <param name="ctx"></param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        public static ValidateContext<decimal> MustGreaterThanOrEuqalToZero(this ValidateContext<decimal> ctx, string errorMessage = null)
        {
            if (ctx.Model < 0)
            {
                ctx.ErrorMessage.Add((ctx.ModelPath, errorMessage ?? $"'{ctx.ModelPath}' 必须大于等于0!"));
            }
            return ctx;
        }
        #endregion
    }
}
