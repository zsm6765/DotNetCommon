using DotNetCommon.Data;
using NUnit.Framework;
using Shouldly;
using System.Collections.Generic;
using System.Linq;

namespace DotNetCommon.Test
{
    [TestFixture]
    public class ResultTests
    {
        [Test]
        public void Test()
        {
            Result.NotOk("error", code: 20002, data: new { }, extData: "errorDetail");
            Result.OkPage<string>(10, new List<string>() { "xiaoming", "С��" });
        }

        public class GetPersonPageReq : PageQuery
        {
            public string Name { get; set; }
        }
        public class Person { public string Name { get; set; } }

        public ResultPage<Person> GetPersonPage(GetPersonPageReq req)
        {
            //req.PageIndex
            //req.PageSize
            //req.Name
            return Result.OkPage(100, Enumerable.Range(1, 10).Select(i => new Person()).ToList());
        }
    }
}
