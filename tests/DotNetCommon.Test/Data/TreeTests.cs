﻿using DotNetCommon.Data;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DotNetCommon.Extensions;

namespace DotNetCommon.Test.Data
{
    [TestFixture]
    public class TreeTests
    {
        public class Area
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public int? PId { get; set; }
            public List<Area> Children { get; set; }
        }

        public class Area2 : ITreeStruct<Area2>
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public int? PId { get; set; }
            public List<Area2> Children { get; set; }
        }

        public class Area3
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public int? PId { get; set; }
        }

        /// <summary>
        /// FetchToTree/ToTree
        /// </summary>
        [Test]
        public void TestToTree()
        {
            var json = "[{\"Id\":1,\"Name\":\"中国\",\"PId\":null,\"Children\":[]},{\"Id\":2,\"Name\":\"河南\",\"PId\":1,\"Children\":[]},{\"Id\":3,\"Name\":\"郑州\",\"PId\":2,\"Children\":[]},{\"Id\":4,\"Name\":\"中原区\",\"PId\":3,\"Children\":[]},{\"Id\":5,\"Name\":\"金水区\",\"PId\":3,\"Children\":[]},{\"Id\":6,\"Name\":\"洛阳\",\"PId\":2,\"Children\":[]},{\"Id\":7,\"Name\":\"西工区\",\"PId\":6,\"Children\":[]},{\"Id\":8,\"Name\":\"湖北\",\"PId\":1,\"Children\":[]},{\"Id\":9,\"Name\":\"武汉\",\"PId\":8,\"Children\":[]},{\"Id\":10,\"Name\":\"十堰\",\"PId\":8,\"Children\":[]},{\"Id\":11,\"Name\":\"山东\",\"PId\":1,\"Children\":[]},{\"Id\":12,\"Name\":\"美国\",\"PId\":null,\"Children\":[]},{\"Id\":13,\"Name\":\"华盛顿州\",\"PId\":12,\"Children\":[]},{\"Id\":14,\"Name\":\"得克萨斯州\",\"PId\":12,\"Children\":[]}]";
            //FetchToTree: 普通
            var list = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Area>>(json);
            var tree = list.FetchToTree(i => i.Id, i => i.PId, i => i.Children, i => i.PId == null || i.PId == 0);

            //FetchToTree: ITreeStruct
            var list2 = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Area2>>(json);
            var tree2 = list2.FetchToTree(i => i.Id, i => i.PId);

            //ToTree: 普通
            var list3 = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Area3>>(json);
            var tree3 = list3.ToTree(i => i.Id, i => i.PId, i => i.PId == null);
            var tree4 = list3.ToTree(i => i.Id, i => i.PId, default(int?));
        }

        /// <summary>
        /// ToFlat
        /// </summary>
        [Test]
        public void TestToFlat()
        {
            var json = "[{\"Id\":1,\"Name\":\"中国\",\"PId\":null,\"Children\":[]},{\"Id\":2,\"Name\":\"河南\",\"PId\":1,\"Children\":[]},{\"Id\":3,\"Name\":\"郑州\",\"PId\":2,\"Children\":[]},{\"Id\":4,\"Name\":\"中原区\",\"PId\":3,\"Children\":[]},{\"Id\":5,\"Name\":\"金水区\",\"PId\":3,\"Children\":[]},{\"Id\":6,\"Name\":\"洛阳\",\"PId\":2,\"Children\":[]},{\"Id\":7,\"Name\":\"西工区\",\"PId\":6,\"Children\":[]},{\"Id\":8,\"Name\":\"湖北\",\"PId\":1,\"Children\":[]},{\"Id\":9,\"Name\":\"武汉\",\"PId\":8,\"Children\":[]},{\"Id\":10,\"Name\":\"十堰\",\"PId\":8,\"Children\":[]},{\"Id\":11,\"Name\":\"山东\",\"PId\":1,\"Children\":[]},{\"Id\":12,\"Name\":\"美国\",\"PId\":null,\"Children\":[]},{\"Id\":13,\"Name\":\"华盛顿州\",\"PId\":12,\"Children\":[]},{\"Id\":14,\"Name\":\"得克萨斯州\",\"PId\":12,\"Children\":[]}]";
            //ToFlat: SetNull/SetEmpty
            var list = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Area>>(json);
            var tree = list.FetchToTree(i => i.Id, i => i.PId, i => i.Children, i => i.PId == null || i.PId == 0);
            var flat = tree.ToFlat(i => i.Children);
            flat = tree.ToFlat(i => i.Children, TreeToFlatAction.SetEmpty);
            flat.ForEach(i =>
            {
                Assert.IsTrue(i.Children == null || i.Children.Count == 0);
            });

            //ToFlat: SetEmptyCollection
            list = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Area>>(json);
            tree = list.FetchToTree(i => i.Id, i => i.PId, i => i.Children, i => i.PId == null || i.PId == 0);
            flat = tree.ToFlat(i => i.Children, TreeToFlatAction.SetEmptyCollection);
            flat.ForEach(i =>
            {
                Assert.IsTrue(i.Children != null && i.Children.Count == 0);
            });

            //ToFlat: SetNull
            list = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Area>>(json);
            tree = list.FetchToTree(i => i.Id, i => i.PId, i => i.Children, i => i.PId == null || i.PId == 0);
            flat = tree.ToFlat(i => i.Children, TreeToFlatAction.SetNull);
            flat.ForEach(i =>
            {
                Assert.IsTrue(i.Children == null);
            });

            //FetchToTree: ITreeStruct
            var list2 = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Area2>>(json);
            var tree2 = list2.FetchToTree(i => i.Id, i => i.PId);
            var flat2 = tree2.ToFlat(i => i.Children);
        }

        /// <summary>
        /// FilterTree
        /// </summary>
        [Test]
        public void TestFilterTree()
        {
            var json = "[{\"Id\":1,\"Name\":\"中国\",\"PId\":null,\"Children\":[]},{\"Id\":2,\"Name\":\"河南\",\"PId\":1,\"Children\":[]},{\"Id\":3,\"Name\":\"郑州\",\"PId\":2,\"Children\":[]},{\"Id\":4,\"Name\":\"中原区\",\"PId\":3,\"Children\":[]},{\"Id\":5,\"Name\":\"金水区\",\"PId\":3,\"Children\":[]},{\"Id\":6,\"Name\":\"洛阳\",\"PId\":2,\"Children\":[]},{\"Id\":7,\"Name\":\"西工区\",\"PId\":6,\"Children\":[]},{\"Id\":8,\"Name\":\"湖北\",\"PId\":1,\"Children\":[]},{\"Id\":9,\"Name\":\"武汉\",\"PId\":8,\"Children\":[]},{\"Id\":10,\"Name\":\"十堰\",\"PId\":8,\"Children\":[]},{\"Id\":11,\"Name\":\"山东\",\"PId\":1,\"Children\":[]},{\"Id\":12,\"Name\":\"美国\",\"PId\":null,\"Children\":[]},{\"Id\":13,\"Name\":\"华盛顿州\",\"PId\":12,\"Children\":[]},{\"Id\":14,\"Name\":\"得克萨斯州\",\"PId\":12,\"Children\":[]}]";
            //FetchToTree: 普通
            var list = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Area>>(json);
            var tree = list.FetchToTree(i => i.Id, i => i.PId, i => i.Children, i => i.PId == null || i.PId == 0);
            var res = tree.ToList().FilterTree(i => i.Children, i => i.Name.Contains("区"));


            list = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Area>>(json);
            tree = list.FetchToTree(i => i.Id, i => i.PId, i => i.Children, i => i.PId == null || i.PId == 0);
            var newTree = tree.FilterTree(i => i.Children, i => i.Name.Contains("河南"));

            list = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Area>>(json);
            tree = list.FetchToTree(i => i.Id, i => i.PId, i => i.Children, i => i.PId == null || i.PId == 0);
            newTree = tree.FilterTree(i => i.Children, i => i.Name.Contains("河南"), true);
        }

        /// <summary>
        /// RecurseTree
        /// </summary>
        [Test]
        public void TestRecurseTree()
        {
            var json = "[{\"Id\":1,\"Name\":\"中国\",\"PId\":null,\"Children\":[]},{\"Id\":2,\"Name\":\"河南\",\"PId\":1,\"Children\":[]},{\"Id\":3,\"Name\":\"郑州\",\"PId\":2,\"Children\":[]},{\"Id\":4,\"Name\":\"中原区\",\"PId\":3,\"Children\":[]},{\"Id\":5,\"Name\":\"金水区\",\"PId\":3,\"Children\":[]},{\"Id\":6,\"Name\":\"洛阳\",\"PId\":2,\"Children\":[]},{\"Id\":7,\"Name\":\"西工区\",\"PId\":6,\"Children\":[]},{\"Id\":8,\"Name\":\"湖北\",\"PId\":1,\"Children\":[]},{\"Id\":9,\"Name\":\"武汉\",\"PId\":8,\"Children\":[]},{\"Id\":10,\"Name\":\"十堰\",\"PId\":8,\"Children\":[]},{\"Id\":11,\"Name\":\"山东\",\"PId\":1,\"Children\":[]},{\"Id\":12,\"Name\":\"美国\",\"PId\":null,\"Children\":[]},{\"Id\":13,\"Name\":\"华盛顿州\",\"PId\":12,\"Children\":[]},{\"Id\":14,\"Name\":\"得克萨斯州\",\"PId\":12,\"Children\":[]}]";
            //FetchToTree: 普通
            var list = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Area>>(json);
            var tree = list.FetchToTree(i => i.Id, i => i.PId, i => i.Children, i => i.PId == null || i.PId == 0).ToList();
            var ids = new List<int>();
            tree = tree.FilterTree(i => i.Children, i => i.Name.Contains("区"), withAllChildren: true);
            var list3 = new List<RecurseTreeContext<Area>>();
            var res = tree.RecurseTree(i => i.Children, ctx =>
            {
                var depart = ctx.Current;
                int deep = ctx.DeepIndex;
                var parents = ctx.Parents;
                bool b = ctx.IsLeaf;
                ctx.BreakRecurse();
                list3.Add(ctx);
            });
        }

        /// <summary>
        /// RecurseTree: BreakRecurse
        /// </summary>
        [Test]
        public void TestRecurseTreeBreak()
        {
            var json = "[{\"Id\":1,\"Name\":\"中国\",\"PId\":null,\"Children\":[]},{\"Id\":2,\"Name\":\"河南\",\"PId\":1,\"Children\":[]},{\"Id\":3,\"Name\":\"郑州\",\"PId\":2,\"Children\":[]},{\"Id\":4,\"Name\":\"中原区\",\"PId\":3,\"Children\":[]},{\"Id\":5,\"Name\":\"金水区\",\"PId\":3,\"Children\":[]},{\"Id\":6,\"Name\":\"洛阳\",\"PId\":2,\"Children\":[]},{\"Id\":7,\"Name\":\"西工区\",\"PId\":6,\"Children\":[]},{\"Id\":8,\"Name\":\"湖北\",\"PId\":1,\"Children\":[]},{\"Id\":9,\"Name\":\"武汉\",\"PId\":8,\"Children\":[]},{\"Id\":10,\"Name\":\"十堰\",\"PId\":8,\"Children\":[]},{\"Id\":11,\"Name\":\"山东\",\"PId\":1,\"Children\":[]},{\"Id\":12,\"Name\":\"美国\",\"PId\":null,\"Children\":[]},{\"Id\":13,\"Name\":\"华盛顿州\",\"PId\":12,\"Children\":[]},{\"Id\":14,\"Name\":\"得克萨斯州\",\"PId\":12,\"Children\":[]}]";
            //FetchToTree: 普通
            var list = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Area>>(json);
            var tree = list.FetchToTree(i => i.Id, i => i.PId, i => i.Children, i => i.PId == null || i.PId == 0).ToList();
            var ids = new List<int>();
            tree = tree.FilterTree(i => i.Children, i => i.Name.Contains("区"));
            var list3 = new List<RecurseTreeContext<Area>>();
            var res = tree.RecurseTree(i => i.Children, ctx =>
            {
                list3.Add(ctx);
                if (ctx.Current.Name == "河南") ctx.BreakRecurse();
            });
        }
    }
}
