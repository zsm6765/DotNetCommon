﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DotNetCommon.Test
{
    [TestFixture]
    public class DistributeGeneratorTests
    {
        [Test]
        public void SnowflakeIdWorkerCtorTests()
        {
            Shouldly.Should.NotThrow(() =>
            {
                new SnowflakeIdWorker(1);
                new SnowflakeIdWorker(0);
                new SnowflakeIdWorker(1023);
                new SnowflakeIdWorker(1, DateTime.Parse("2021-01-01"));
                new SnowflakeIdWorker(1, null, timeLength: 44);
            });

            //机器Id和机器Id的bit位数不匹配
            Shouldly.Should.Throw<Exception>(() => new SnowflakeIdWorker(1024));
            //起始时间必须大于2020年
            Shouldly.Should.Throw<Exception>(() => new SnowflakeIdWorker(1, DateTime.Parse("2019-01-01")));
            //时间戳取值: 28-44
            Shouldly.Should.Throw<Exception>(() => new SnowflakeIdWorker(1, timeLength: 45));
            Shouldly.Should.Throw<Exception>(() => new SnowflakeIdWorker(1, timeLength: 27));
            //机器id位数: 5-18
            Shouldly.Should.Throw<Exception>(() => new SnowflakeIdWorker(1, machineLength: 4));
            Shouldly.Should.Throw<Exception>(() => new SnowflakeIdWorker(1, machineLength: 19));
            //序列号位数: 2-24
            Shouldly.Should.Throw<Exception>(() => new SnowflakeIdWorker(1, sequenceLength: 1));
            Shouldly.Should.Throw<Exception>(() => new SnowflakeIdWorker(1, sequenceLength: 25));

        }

        [Test]
        public void SnowflakeIdWorkerTests()
        {
            //雪花算法：连续生成2万个id
            Shouldly.Should.NotThrow(() =>
            {
                var snow = new SnowflakeIdWorker(1);
                var dic = new Dictionary<long, string>();
                for (var i = 0; i < 20000; i++)
                {
                    dic.Add(snow.NextId(), null);
                }
                var dic2 = new Dictionary<long, string>();
                foreach (var i in dic)
                {
                    var id = i.Key;
                    dic2[id] = snow.AnalyzeId(id);
                }
            });

            //起始时间是未来时间
            Shouldly.Should.NotThrow(() =>
            {
                var snow = new SnowflakeIdWorker(1, startTime: DateTime.Parse("2022-01-01"));
                var dic = new Dictionary<long, string>();
                for (var i = 0; i < 20000; i++)
                {
                    dic.Add(snow.NextId(), null);
                }
                var dic2 = new Dictionary<long, string>();
                foreach (var i in dic)
                {
                    var id = i.Key;
                    dic2[id] = snow.AnalyzeId(id);
                }
            });
        }

        [Test]
        public void IdTest()
        {
            //给表test生成id
            var testids = new List<long>();
            var test2ids = new List<long>();
            var test3ids = new List<long>();
            for (int i = 0; i < 100; i++)
            {
                testids.Add(DistributeGenerator.NewId("test"));
                test2ids.Add(DistributeGenerator.NewId("test2"));
                test3ids.Add(DistributeGenerator.NewId("test3"));
            }
            Assert.IsTrue(testids.Count == 100 && test2ids.Count == 100 && test3ids.Count == 100);
            Assert.IsTrue(testids.Distinct().Count() == 100);
            Assert.IsTrue(test2ids.Distinct().Count() == 100);
            Assert.IsTrue(test3ids.Distinct().Count() == 100);
            var testids2 = testids.OrderBy(l => l).ToList();
            Assert.IsTrue(testids2.SequenceEqual(testids));
            var test2ids2 = test2ids.OrderBy(l => l).ToList();
            Assert.IsTrue(test2ids2.SequenceEqual(test2ids));
            var test3ids2 = test3ids.OrderBy(l => l).ToList();
            Assert.IsTrue(test3ids2.SequenceEqual(test3ids));
        }

        [Test]
        public void MachineTxtMachineCode()
        {
            //将machine.txt文件放到DotNetCommon.dll同目录下
            /** machine.txt 文件内容
            MachineId=12
            MachineIdString=ABCD
             * */
            Machine.SetMachineId(11, null);
            Assert.IsTrue(Machine.MachineId == 11);
            Assert.IsTrue(Machine.MachineIdString == "11");

            Machine.SetMachineFlagByDefault();
            Assert.IsTrue(Machine.MachineId == 12);
            Assert.IsTrue(Machine.MachineIdString == "ABCD");
        }

        [Test]
        public void EnvConfigMachineCode()
        {
            /** 系统环境配置 注意修改系统变量后重启vs
             * MachineId=5
             * */
            Machine.SetMachineId(5, null);
            Assert.IsTrue(Machine.MachineId == 12);
        }

        [Test]
        public void SNOTest()
        {
            var nos = new List<string>();
            var nos2 = new List<string>();
            var nos3 = new List<string>();
            var nos4 = new List<string>();
            for (int i = 0; i < 100; i++)
            {
                nos.Add(DistributeGenerator.NewSNO("testdb_order", SerialFormat.CreateFast("ORDER-", "yyyyMMdd", 6, true)));
                nos2.Add(DistributeGenerator.NewSNO("testdb_order2", SerialFormat.CreateFast("ORDER-", "yyyyMMdd", 6, true)));
                nos3.Add(DistributeGenerator.NewSNO("testdb_studentno", SerialFormat.CreateFast("SNO", "yyyyMMdd", 6, true)));
                nos4.Add(DistributeGenerator.NewSNO("testdb_order4", SerialFormat.CreateByChunks(new List<SerialFormatChunk>()
                {
                    new SerialFormatChunk()
                    {
                        Type=SerialFormatChunkType.MachineText
                    },
                    new SerialFormatChunk()
                    {
                        Type=SerialFormatChunkType.StaticText,
                        FormatString="-"
                    },
                    new SerialFormatChunk()
                    {
                        Type=SerialFormatChunkType.DateText,
                        FormatString="yyyyMMdd"
                    },
                    new SerialFormatChunk()
                    {
                        Type=SerialFormatChunkType.StaticText,
                        FormatString="-"
                    },
                    new SerialFormatChunk()
                    {
                        Type=SerialFormatChunkType.SerialNo,
                        Length=4
                    }
                })));
                nos4.Add(DistributeGenerator.NewSNO("testdb_order5", SerialFormat.CreateByChunks(new List<SerialFormatChunk>()
                {
                    new SerialFormatChunk()
                    {
                        Type=SerialFormatChunkType.DateText,
                        FormatString="yyyyMMdd"
                    },
                    new SerialFormatChunk()
                    {
                        Type=SerialFormatChunkType.SerialNo,
                        Length=4
                    }
                })));
            }
            Assert.IsTrue(nos.Count == 100 && nos2.Count == 100 && nos3.Count == 100);
            Assert.IsTrue(nos.Distinct().Count() == 100);
            Assert.IsTrue(nos2.Distinct().Count() == 100);
            Assert.IsTrue(nos3.Distinct().Count() == 100);
            var nos_2 = nos.OrderBy(l => l).ToList();
            Assert.IsTrue(nos_2.SequenceEqual(nos));
            var nos2_2 = nos2.OrderBy(l => l).ToList();
            Assert.IsTrue(nos2_2.SequenceEqual(nos2));
            var nos3_2 = nos3.OrderBy(l => l).ToList();
            Assert.IsTrue(nos3_2.SequenceEqual(nos3));
        }
    }
}
