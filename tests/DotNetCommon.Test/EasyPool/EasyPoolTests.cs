﻿namespace DotNetCommon.Test.EasyPool
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using NUnit.Framework;
    using Shouldly;

    [TestFixture]
    internal sealed class EasyPoolTests
    {
        [Test]
        public void When_creating_a_pool_with_null_factory()
        {
            var ex = Should.Throw<ArgumentNullException>(() => new EasyPool<string>(null, null, 10));
            ex.Message.ShouldBe("Value cannot be null. (Parameter 'factory')");
            ex.ParamName.ShouldBe("factory");
        }

        [Test]
        public void When_returning_more_than_max_pool_size()
        {
            Func<SomeObject> factory = () => new SomeObject();
            const int MaxCount = 1;
            using (IEasyPool<SomeObject> pool = new EasyPool<SomeObject>(factory, null, MaxCount))
            {
                pool.Count.ShouldBe(0);

                var obj1 = new SomeObject();

                pool.Return(obj1).ShouldBeTrue();

                pool.Count.ShouldBe(1);

                var obj2 = new SomeObject();

                pool.Return(obj2).ShouldBeFalse();

                pool.Count.ShouldBe(1);

                var pooledObj = pool.Get();
                pooledObj.ShouldBe(obj1);

                pool.Count.ShouldBe(0);
            }
        }

        [Test]
        public void When_getting_and_renting_items_from_the_pool_with_null_reset()
        {
            Func<SomeObject> factory = () => new SomeObject { Name = "Foo", Number = 1 };

            using (IEasyPool<SomeObject> pool = new EasyPool<SomeObject>(factory, null, 5))
            {
                pool.Count.ShouldBe(0);

                var obj1 = pool.Get();

                obj1.ShouldNotBeNull();
                obj1.Name.ShouldBe("Foo");
                obj1.Number.ShouldBe(1);

                pool.Count.ShouldBe(0);

                pool.Return(obj1).ShouldBeTrue();

                pool.Count.ShouldBe(1);

                var obj2 = pool.Get();

                obj2.ShouldNotBeNull();
                obj2.ShouldBe(obj1);

                obj2.Name.ShouldBe("Foo");
                obj2.Number.ShouldBe(1);
            }
        }

        [Test]
        public void When_getting_and_renting_items_from_the_pool_with_reset()
        {
            Func<SomeObject> factory = () => new SomeObject { Name = "Foo", Number = 1 };
            Action<SomeObject> reset = o =>
            {
                o.Name = null;
                o.Number = 0;
            };

            using (IEasyPool<SomeObject> pool = new EasyPool<SomeObject>(factory, reset, 5))
            {
                pool.Count.ShouldBe(0);

                var obj1 = pool.Get();

                obj1.ShouldNotBeNull();
                obj1.Name.ShouldBe("Foo");
                obj1.Number.ShouldBe(1);

                pool.Count.ShouldBe(0);

                pool.Return(obj1).ShouldBeTrue();

                pool.Count.ShouldBe(1);

                var obj2 = pool.Get();

                obj2.ShouldNotBeNull();
                obj2.ShouldBe(obj1);

                obj2.Name.ShouldBeNull();
                obj2.Number.ShouldBe(0);
            }
        }

        [Test]
        public void When_getting_and_renting_items_from_the_pool_with_no_reset_on_return()
        {
            Func<SomeObject> factory = () => new SomeObject { Name = "Foo", Number = 1 };
            Action<SomeObject> reset = o =>
            {
                o.Name = null;
                o.Number = 0;
            };

            using (IEasyPool<SomeObject> pool = new EasyPool<SomeObject>(factory, reset, 5))
            {
                pool.Count.ShouldBe(0);

                var obj1 = pool.Get();

                obj1.ShouldNotBeNull();
                obj1.Name.ShouldBe("Foo");
                obj1.Number.ShouldBe(1);

                pool.Count.ShouldBe(0);

                pool.Return(obj1, false).ShouldBeTrue();

                pool.Count.ShouldBe(1);

                var obj2 = pool.Get();

                obj2.ShouldNotBeNull();
                obj2.ShouldBe(obj1);

                obj2.Name.ShouldBe("Foo");
                obj2.Number.ShouldBe(1);
            }
        }

        [Test]
        public void When_disposing_the_pool()
        {
            Func<SomeObject> factory = () => new SomeObject();
            const int MaxCount = 1;
            IEasyPool<SomeObject> pool = new EasyPool<SomeObject>(factory, null, MaxCount);
            var obj1 = new SomeObject();

            pool.Return(obj1).ShouldBeTrue();
            pool.Count.ShouldBe(1);

            pool.Dispose();
            pool.Count.ShouldBe(0);
        }

        [Test]
        public void Test()
        {
            var id = 1;
            //创建对象池,指定实例构建逻辑、归还后对实例的清洗逻辑、池中存放的最大实例数量
            var pool = new EasyPool<TestObject>(() => new TestObject(id++), obj => obj.Reset(), 5);
            //池中还没有实例
            pool.Count.ShouldBe(0);

            //从池中获取实例，这将触发实例构建逻辑
            var obj = pool.Get();
            //池中还是没有实例
            pool.Count.ShouldBe(0);
            //将实例归还到池中，并触发清洗逻辑
            //输出：2021-06-05 22:21:35.746 1: 触发了清洗方法!
            pool.Return(obj);
            //此时池中应该有一个实例
            pool.Count.ShouldBe(1);

            //从池中连续获取8个实例
            var list = new List<TestObject>();
            for (var i = 0; i < 8; i++)
            {
                list.Add(pool.Get());
            }
            //此时池中没有实例
            pool.Count.ShouldBe(0);
            //将实例全部归还到池中
            //连续输出5次清洗方法
            //然后连续交替输出3次清洗和Dispose方法
            list.ForEach(item => pool.Return(item));
            //因为池中最多缓存5个实例，所以归还的前5个实例被放进了池子里，后面的3个未放进去并且额外触发了Dispose方法（如果有Dispose方法的话）
            pool.Count.ShouldBe(5);
            //销毁池子,依次调用实例的Dispose方法
            pool.Dispose();

        }

        private sealed class SomeObject
        {
            public string Name { get; set; }
            public int Number { get; set; }
        }

        private class TestObject : IDisposable
        {
            public TestObject(int id)
            {
                Id = id;
            }
            public int Id { get; set; }
            public string Name { get; set; }

            public void Dispose()
            {
                Console.WriteLine($"{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff")} {Id}: 触发了Dispose方法!");
                //Debug.WriteLine($"{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff")} {Id}: 触发了Dispose方法!");
            }

            public void Reset()
            {
                Console.WriteLine($"{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff")} {Id}: 触发了清洗方法!");
                //Debug.WriteLine($"{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff")} {Id}: 触发了清洗方法!");
            }
        }
    }
}