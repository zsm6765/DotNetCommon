﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using DotNetCommon.Extensions;
using Shouldly;

namespace DotNetCommon.Test.Extensions
{
    [TestFixture]
    public class DateTimeExtensionsTests
    {
        [Test]
        public void ToCommonStringTest()
        {
            var dt = new DateTime(2021, 2, 2, 13, 45, 02, 123);
            var str = dt.ToCommonString();
            str.ShouldBe("2021-02-02 13:45:02.123");
            str = dt.ToCommonDateString();
            str.ShouldBe("2021-02-02");
            str = dt.ToCommonDateTimeString();
            str.ShouldBe("2021-02-02 13:45:02");
        }

        [Test]
        public void Test()
        {
            //Age
            //只有过了生日当前年龄才会涨1
            var dt = new DateTime(1991, 01, 01);
            var age = dt.Age();
            age.ShouldBeGreaterThanOrEqualTo(30);

            //IsLeapYear
            //1880年是闰年,1881年不是
            dt = new DateTime(1880, 01, 01);
            dt.IsLeapYear().ShouldBeTrue();
            dt = new DateTime(1881, 01, 01);
            dt.IsLeapYear().ShouldBeFalse();

            //IsWeekend
            //2021-06-19：周六
            dt = new DateTime(2021,6,19);
            dt.IsWeekend().ShouldBeTrue();

            //2021-06-20：周日
            dt = new DateTime(2021, 6, 20);
            dt.IsWeekend().ShouldBeTrue();

            //2021-06-21：周一
            dt = new DateTime(2021, 6, 21);
            dt.IsWeekend().ShouldBeFalse();

            //IsLastDayOfTheMonth
            dt = new DateTime(2021,06,30);
            dt.IsLastDayOfTheMonth().ShouldBeTrue();

            dt = new DateTime(2021, 05, 30);
            dt.IsLastDayOfTheMonth().ShouldBeFalse();

            //IsBetween
            dt = new DateTime(2020, 05, 02);
            dt.IsBetween(new DateTime(2020, 05, 01), new DateTime(2020, 05, 03)).ShouldBeTrue();
        }
    }
}
