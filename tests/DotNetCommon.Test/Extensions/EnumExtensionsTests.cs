﻿using NUnit.Framework;
using Shouldly;
using System;
using System.Collections.Generic;
using System.Text;
using DotNetCommon.Extensions;

namespace DotNetCommon.Test.Extensions
{
    [TestFixture]
    public sealed class EnumExtensionsTests
    {
        [Test]
        public void EnumToDescriptionTest()
        {
            //普通枚举无特性
            EnumTest test = EnumTest.B;
            test.ToDescription().ShouldBe("B");

            //普通枚举有特性
            var test2 = EnumTest2.B;
            test2.ToDescription().ShouldBe("测试描述");
            test2 = EnumTest2.C;
            test2.ToDescription().ShouldBe("C");

            //位枚举无特性
            var test3 = EnumTest3.B | EnumTest3.C;
            test3.ToDescription().ShouldBe("B, C");

            //位枚举有特性
            var test4 = EnumTest4.B | EnumTest4.C;
            test4.ToDescription().ShouldBe("这是B, 这是C");

            //位枚举混合特性
            var test5 = EnumTest4.A | EnumTest4.B | EnumTest4.C;
            test5.ToDescription().ShouldBe("A, 这是B, 这是C");
        }

        /// <summary>
        /// ToCodeKeyDescriptionList
        /// </summary>
        [Test]
        public void ToCodeKeyDescriptionListTest()
        {
            EnumTest2 test = EnumTest2.B;
            var list = test.ToCodeKeyDescriptionList();
            list.Count.ShouldBe(3);
            list[0].code.ShouldBe(0);
            list[0].key.ShouldBe("A");
            list[0].desc.ShouldBe("A");

            list[1].code.ShouldBe(1);
            list[1].key.ShouldBe("B");
            list[1].desc.ShouldBe("测试描述");

            list[2].code.ShouldBe(2);
            list[2].key.ShouldBe("C");
            list[2].desc.ShouldBe("C");

            list = test.ToCodeKeyDescriptionList(false);
            list.Count.ShouldBe(3);
            list[0].code.ShouldBe(0);
            list[0].key.ShouldBe("A");
            list[0].desc.ShouldBe("");

            list[1].code.ShouldBe(1);
            list[1].key.ShouldBe("B");
            list[1].desc.ShouldBe("测试描述");

            list[2].code.ShouldBe(2);
            list[2].key.ShouldBe("C");
            list[2].desc.ShouldBe("");
        }

        /// <summary>
        /// ToCodeDescriptionList
        /// </summary>
        [Test]
        public void ToCodeDescriptionListTest()
        {
            EnumTest2 test = EnumTest2.B;
            var list = test.ToCodeDescriptionList();
            list.Count.ShouldBe(3);
            list[0].Key.ShouldBe(0);
            list[0].Value.ShouldBe("A");

            list[1].Key.ShouldBe(1);
            list[1].Value.ShouldBe("测试描述");

            list[2].Key.ShouldBe(2);
            list[2].Value.ShouldBe("C");

            list = test.ToCodeDescriptionList(false);
            list.Count.ShouldBe(3);
            list[0].Key.ShouldBe(0);
            list[0].Value.ShouldBe("");

            list[1].Key.ShouldBe(1);
            list[1].Value.ShouldBe("测试描述");

            list[2].Key.ShouldBe(2);
            list[2].Value.ShouldBe("");
        }
    }

    public enum EnumTest
    {
        A, B, C
    }
    public enum EnumTest2
    {
        A,
        [System.ComponentModel.Description("测试描述")]
        B,
        C
    }

    [Flags]
    public enum EnumTest3
    {
        A = 1, B = 2, C = 4
    }

    [Flags]
    public enum EnumTest4
    {
        A = 1,
        [System.ComponentModel.Description("这是B")]
        B = 2,
        [System.ComponentModel.Description("这是C")]
        C = 4
    }
}
