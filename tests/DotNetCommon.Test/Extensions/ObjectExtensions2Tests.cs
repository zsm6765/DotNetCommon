﻿using NUnit.Framework;
using Shouldly;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using DotNetCommon.Extensions;

namespace DotNetCommon.Test.Extensions
{
    [TestFixture]
    public class ObjectExtensions2Tests
    {
        [Test]
        public void Test()
        {
            int? num = null;
            num.IfNullOrDefaultUse(5).ShouldBe(5);
            num.IfNullOrDefaultUse().ShouldBe(0);

            var num2 = 0;
            num2.IfDefaultUse(2).ShouldBe(2);

            bool? flag = null;
            flag.IfNullUse().ShouldBe(false);
            flag.IfNullUse(true).ShouldBe(true);

            byte? b = null;
            b.IfNullOrDefaultUse(5);


            object obj = null;
            obj.IfNullUse(new object()).ShouldNotBeNull();
        }
        [Test]
        public void Test2()
        {
            //集合null
            List<int> nums = null;
            nums.IfNullUseNewList().Count.ShouldBe(0);

            //数组null
            int[] ints = null;
            ints.IfNullUseNewArray().Length.ShouldBe(0);
            ints.IfNullUseNewList().Count.ShouldBe(0);

            //字典null
            Dictionary<string, string> dic = null;
            dic.IfNullUseNewDictinoary().Count.ShouldBe(0);

            //字符串
            string str = null;
            str.IfNullOrEmptyUse("haha").ShouldBe("haha");
            str = "";
            str.IfNullOrEmptyUse("hehe").ShouldBe("hehe");
        }

        [Test]
        public void Test3()
        {
            var dt = DateTime.Parse("2021-05-09 23:16:03.503");
            var res = dt.To<DateTime>();
            res = "2021-05-09 23:16:03.503".To<DateTime>();


            var dt2 = DateTimeOffset.Parse("2021-05-09 23:16:03.503");
            res = dt2.To<DateTime>();
            res = "2021-05-09 23:16:03.503".To<DateTime>();
        }

        [Test]
        public void StringToOther()
        {
            var str = "1.2";
            //字符串转数字
            str.To<double>().ShouldBe(1.2);

            str = null;
            str.To<double?>().ShouldBe(null);

            str = "12";
            str.To<int>().ShouldBe(12);

            //字符串转日期
            str = "2020-01-01";
            str.To<DateTime>().ShouldBe(DateTime.Parse("2020-01-01"));
            str = null;
            str.To<DateTime?>().ShouldBe(null);

            //字符串转枚举
            str = "Close";
            str.To<EnumState>().ShouldBe(EnumState.Close);
            str = "Close,Active";
            var state = str.To<EnumState>();
            state.Contains(EnumState.Close).ShouldBeTrue();
            state.Contains(EnumState.Active).ShouldBeTrue();
            state.Contains(EnumState.Open).ShouldBeFalse();

            //字符串转bool
            str = "true";
            str.To<bool>().ShouldBeTrue();
            str = "True";
            str.To<bool>().ShouldBeTrue();
            str = "false";
            str.To<bool>().ShouldBeFalse();
            str = "False";
            str.To<bool>().ShouldBeFalse();

            str = null;
            str.To<bool?>().ShouldBeNull();
        }

        [Flags]
        public enum EnumState { Open = 1, Close = 2, Active = 4 }

        [Test]
        public void NumberToOtherTest()
        {
            int i = 2;
            //数字转枚举
            i.To<EnumState>().ShouldBe(EnumState.Close);

            i = 3;
            i.To<EnumState>().ShouldBe(EnumState.Open | EnumState.Close);
        }

        [Test]
        public void TestFail()
        {
            var str = "lp1";
            str.ToWithCustomFallback<int>().ShouldBe(default(int));
            str.ToWithCustomFallback<int>(2).ShouldBe(2);
        }

    }

}
