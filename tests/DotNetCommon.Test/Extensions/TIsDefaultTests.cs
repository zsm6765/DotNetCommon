﻿using DotNetCommon.Extensions;
using NUnit.Framework;
using Shouldly;
using System;
using System.Collections.Generic;
using System.Text;

namespace DotNetCommon.Test.Extensions
{
    [TestFixture]
    public class TIsDefaultTests
    {
        [Test]
        public void Test()
        {
            int? i = null;
            i.IsDefault().ShouldBeTrue();

            int j = 0;
            j.IsDefault().ShouldBeTrue();

            object obj = null;
            obj.IsDefault().ShouldBeTrue();
        }
    }
}
