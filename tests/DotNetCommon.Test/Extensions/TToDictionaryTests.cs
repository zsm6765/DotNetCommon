﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using DotNetCommon.Extensions;
using Shouldly;

namespace DotNetCommon.Test.Extensions
{
    [TestFixture]
    public class TToDictionaryTests
    {
        [Test]
        public void Test()
        {
            var obj = new { Id = 1, Name = "小明", Age = 20 };
            var dic = obj.ToDictionary(true);
            dic.ShouldBe(new Dictionary<string, object>() { { "Id", 1 }, { "Name", "小明" }, { "Age", 20 } });

            var person = new Person()
            {
                Id = 1,
                Name = "小明"
            };
            dic = person.ToDictionary(true);
            dic.ShouldBe(new Dictionary<string, object>() { { "Id", 1 }, { "Name", "小明" } });
        }

        public class Person
        {
            public int Id { get; set; }
            public string Name { get; set; }
        }
    }
}
