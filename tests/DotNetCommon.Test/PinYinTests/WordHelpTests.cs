﻿using NUnit.Framework;
using Shouldly;
using System;
using System.Collections.Generic;
using System.Text;
using helper = DotNetCommon.Pinyin.WordsHelper;

namespace DotNetCommon.Test.PinYinTests
{
    [TestFixture]
    public class WordHelpTests
    {
        [Test]
        public void GetPinyinTest()
        {
            var str = helper.GetPinyin("王小明");
            str.ShouldBe("WangXiaoMing");

            //多音字
            str = helper.GetPinyin("银行");
            str.ShouldBe("YinHang");
            str = helper.GetPinyin("行走");
            str.ShouldBe("XingZou");

            //多音字Bug
            str = helper.GetPinyin("我的笔记本");
            //str="WoDiBiJiBen";  "WoDeBiJiBen"
            str = helper.GetPinyin("什么");
            //"ShiYao" "ShenMe"

            //带拼音
            str = helper.GetPinyin("王小明", true);
            str.ShouldBe("WángXiǎoMíng");

            //带分隔符
            str = helper.GetPinyin("王小明", ",");
            str.ShouldBe("Wang,Xiao,Ming");

            //带英文
            str = helper.GetPinyin("王小明how old are you?");
            str.ShouldBe("WangXiaoMinghow old are you?");
        }

        [Test]
        public void GetAllPinyinTest()
        {
            var str = helper.GetAllPinyin('我');
            str.Count.ShouldBe(1);
            str[0].ShouldBe("Wo");
            str = helper.GetAllPinyin('的');
            str.Count.ShouldBe(2);
            str[1].ShouldBe("Di");
            str[1].ShouldBe("De");
            str = helper.GetAllPinyin('行', true);
            str.Count.ShouldBe(5);
        }

        [Test]
        public void GetFirstPinyinTest()
        {
            var str = helper.GetFirstPinyin("我们大家");
            str.ShouldBe("WMDJ");
        }

        [Test]
        public void GetPinyinForNameTest()
        {
            var str = helper.GetPinyinForName("令狐冲");
            str.ShouldBe("LingHuChong");

            str = helper.GetPinyinForName("乐毅");
            str.ShouldBe("YueYi");
            str = helper.GetPinyin("乐毅");
            str.ShouldBe("LeYi");
            str = helper.GetPinyinForName("快乐");
            str.ShouldBe("KuaiLe");
        }

        [Test]
        public void GetPinyinListTest()
        {
            var list = helper.GetPinyinList("银行");
            list.Length.ShouldBe(2);
            list[0].ShouldBe("Yin");
            list[1].ShouldBe("Hang");

            list = helper.GetPinyinList("王小明 How old are you?");
            //... 字母都拆开了
        }

        [Test]
        public void ChineseJudgeTest()
        {
            var b = helper.HasChinese("小明ajdioafahsd");
            b.ShouldBeTrue();

            b = helper.HasChinese("Hi! NBA I'm coming...");
            b.ShouldBeFalse();

            b = helper.HasChinese("，、；");
            b.ShouldBeFalse();

            b = helper.IsAllChinese("我们123");
            b.ShouldBeFalse();

            b = helper.IsAllChinese("我们都是");
            b.ShouldBeTrue();

            b = helper.IsAllChinese("我们都是，");
            b.ShouldBeFalse();

            b = helper.IsAllChinese("我们都是nj");
            b.ShouldBeFalse();
        }
    }
}
