﻿using Newtonsoft.Json.Linq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

using helper = DotNetCommon.RegistryHelper;
namespace DotNetCommon.Test
{
    [TestFixture]
    public class RegistryHelperTests
    {
        [Test]
        public void Test()
        {
            var path = "HKEY_CURRENT_USER\\TestApplication";
            var path2 = path + "\\Res";
            //删除值
            Assert.DoesNotThrow(() => helper.DeletePath(path));
            Assert.IsFalse(helper.Exists(path));
            //写入值
            Assert.DoesNotThrow(() => helper.SetString("HKEY_CURRENT_USER\\TestApplication", "name", "测试应用程序"));
            Assert.DoesNotThrow(() => helper.SetInt(path, "version", 65));
            Assert.DoesNotThrow(() => helper.SetDefault(path2, "默认的"));
            //列出
            var keys = helper.ListDataKeys(path);
            Assert.IsTrue(keys.Count == 2);
            var keys2 = helper.ListDataKeys(path2);
            //判断是否存在
            Assert.IsTrue(helper.Exists(path, "name"));
            Assert.IsFalse(helper.Exists(path, "abcdefg"));
            Assert.IsTrue(helper.Exists(path2));
            //删除
            Assert.DoesNotThrow(() => helper.DeletePath(path2));
            Assert.IsFalse(helper.Exists(path2));
            Assert.DoesNotThrow(() => helper.DeletePath(path));
            Assert.IsFalse(helper.Exists(path));
        }

        [Test]
        public void Test2()
        {
            var path = @"HKEY_CURRENT_USER\TestApplication\Res";
            //判断是否存在
            var b = RegistryHelper.Exists(path);
            //删除项
            RegistryHelper.DeletePath(path);
            //设置值
            RegistryHelper.SetString(path, "name", "小明");
            //读取值
            var name = RegistryHelper.GetString(path, "name");
            //创建注册表项(仅创建路径)
            path = @"HKEY_CURRENT_USER\demo2";
            RegistryHelper.CreatePath(path);
            RegistryHelper.SetString(path, "name", "小明");
            RegistryHelper.CreatePath(path);
        }
    }
}
