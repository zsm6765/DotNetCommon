﻿using DotNetCommon.Validate;
using NUnit.Framework;
using Shouldly;
using System;
using System.Collections.Generic;
using System.Text;

namespace DotNetCommon.Test.Validate
{
    [TestFixture]
    public class WithDisplayNameTests
    {
        [Test]
        public void Test()
        {
            var person = new Person()
            {
                Id = 1,
                Name = "小明",
                Book = new Book()
                {
                    Id = 1,
                    Name = "数学"
                },
                Books = new List<Book>()
                {
                    new Book()
                    {
                        Id=1
                    }
                }
            };
            //'主键' 不能等于 '1'!
            //'Book.课程' 不能等于 '数学'!
            //'书籍列表[0].PK' 不能等于 '1'!
            var msg = ValidateModelHelper.ValidErrorMessage(person, ctx =>
             {
                 ctx.RuleFor(i => i.Id).WithDisplayName("主键").MustNotEqualTo(1);
                 ctx.RuleFor(i => i.Book).RuleFor(i => i.Name).WithDisplayName("课程").MustNotEqualTo("数学");
                 ctx.RuleFor(i => i.Books).WithDisplayName("书籍列表").RuleForEach(cbook =>
                 {
                     cbook.RuleFor(i => i.Id).WithDisplayName("PK").MustNotEqualTo(1);
                 });
             });
            msg.ShouldContain("'主键' 不能等于 '1'!");
            msg.ShouldContain("'Book.课程' 不能等于 '数学'!");
            msg.ShouldContain("'书籍列表[0].PK' 不能等于 '1'!");
        }

        public class Person
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public Book Book { get; set; }
            public List<Book> Books { set; get; }
        }

        public class Book
        {
            public int Id { get; set; }
            public string Name { get; set; }
        }
    }
}
